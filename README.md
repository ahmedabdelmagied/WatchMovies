# MovieShow
- Android application to help users discover popular and highly rated movies on the web.
- This app utilizes core Android user interface components and fetches movies
information using themoviedb.org web API.
 
# ScreenShots
<p align="center">
<img src="https://user-images.githubusercontent.com/17562667/43037759-b74852de-8d11-11e8-803b-0e54000aa20a.png" width="200" height="420">
<img src="https://user-images.githubusercontent.com/17562667/43037770-e46c0a4e-8d11-11e8-9a36-0d6ca0f7e3f0.png" width="200" height="420">

<img src="https://user-images.githubusercontent.com/17562667/43037771-e47b53be-8d11-11e8-8ee8-03f68380982c.png" width="500" height="420">
<img src="https://user-images.githubusercontent.com/17562667/43037772-e51bbbba-8d11-11e8-9567-1d6a0e1b425e.png" width="200" height="420">

<img src="https://user-images.githubusercontent.com/17562667/43037773-e681797c-8d11-11e8-8981-02f32b002a38.png" width="200" height="420">

<img src="https://user-images.githubusercontent.com/17562667/43037774-e8d3c090-8d11-11e8-9d10-51525765442a.png" width="200" height="420">
<img src="https://user-images.githubusercontent.com/17562667/43037776-ea17c8d4-8d11-11e8-909b-93ce460edfa6.png" width="200" height="420">
<img src="https://user-images.githubusercontent.com/17562667/43037778-ecb29448-8d11-11e8-8980-da0149b4ffa2.png" width="200" height="420">

<p>
