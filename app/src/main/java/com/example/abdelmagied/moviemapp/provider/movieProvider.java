package com.example.abdelmagied.moviemapp.provider;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.Nullable;

/**
 * Created by AbdELMagied on 9/25/2017.
 */
public class movieProvider extends ContentProvider {


    public static final int MOVIE          = 101;
    public static final int MOVIE_WITH_ID  = 105;
    public static UriMatcher mUrimatcher    = bulidUriMatcher();
    private movieDb mMovieDatabase;


    public static UriMatcher bulidUriMatcher()
    {
        UriMatcher urimatcher = new UriMatcher(UriMatcher.NO_MATCH);
        urimatcher.addURI(movieContract.AUTHORITY , movieContract.MOVIE_PATH , MOVIE);
        urimatcher.addURI(movieContract.AUTHORITY , movieContract.MOVIE_PATH + "/#" , MOVIE_WITH_ID);

        return urimatcher;
    }



    @Override
    public boolean onCreate() {
        mMovieDatabase = new movieDb(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] strings, String s, String[] strings1, String s1) {
        SQLiteDatabase sqlitedatabase = mMovieDatabase.getReadableDatabase();
        int matcher = mUrimatcher.match(uri);
        Cursor mycursor;
        switch (matcher){
            case MOVIE :
                 mycursor = sqlitedatabase.query(movieContract.MovieEntry.TABLE_NAME,
                        strings,
                        s,
                        strings1,
                        null,
                        null,
                        s1);
                  break;
            case MOVIE_WITH_ID:
                mycursor = sqlitedatabase.query(movieContract.MovieEntry.TABLE_NAME,
                        strings,
                        "_id = ?",
                        new String[]{uri.getPathSegments().get(1)},
                        null,
                        null,
                        s1);
                  break;
            default:
                throw new UnsupportedOperationException("Unknown Uri " + uri);
        }
        mycursor.setNotificationUri(getContext().getContentResolver() , uri);
        return mycursor;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues contentValues) {
        SQLiteDatabase sqLiteDatabase = mMovieDatabase.getWritableDatabase();
        int matcher = mUrimatcher.match(uri);
        Uri rUri ;
        switch (matcher)
        {
            case MOVIE:
                   long insertedId = sqLiteDatabase.insert(movieContract.MovieEntry.TABLE_NAME , null , contentValues);

                    if(insertedId > 0)
                    {
                           rUri = ContentUris.withAppendedId(movieContract.MovieEntry.CONTENT_URI , insertedId);
                    }else{
                        throw new android.database.SQLException("failed to insert " + uri);
                    }
                break;
            default:
                throw new UnsupportedOperationException("Unknown Uri :  " + uri);
        }
        getContext().getContentResolver().notifyChange(uri , null);
        return rUri;
    }

    @Override
    public int delete(Uri uri, String s, String[] strings) {
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues contentValues, String s, String[] strings) {
        return 0;
    }
}
