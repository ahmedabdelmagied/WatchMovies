package com.example.abdelmagied.moviemapp;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by AbdELMagied on 9/23/2017.
 */
public class movieDetails implements Parcelable {

    private String originalTitle;
    private String moviePoster;
    private String plotSynopsis;
    private int movieId;
    private int userRating;
    private String releaseDate;
    private String movieType ;

    public movieDetails(String originalTitle, String moviePoster, String plotSynopsis, int userRating, String releaseDate, int movieId , String movieType) {
        this.originalTitle = originalTitle;
        this.moviePoster   = moviePoster;
        this.plotSynopsis  = plotSynopsis;
        this.userRating    = userRating;
        this.releaseDate   = releaseDate;
        this.movieId       = movieId;
        this.movieType     = movieType;
    }

    private movieDetails(Parcel parcel) {
        originalTitle  = parcel.readString();
        moviePoster    = parcel.readString();
        plotSynopsis   = parcel.readString();
        userRating     = parcel.readInt();
        releaseDate    = parcel.readString();
        movieId        = parcel.readInt();
        movieType      = parcel.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {

        parcel.writeString(originalTitle);
        parcel.writeString(moviePoster);
        parcel.writeString(plotSynopsis);
        parcel.writeInt(userRating);
        parcel.writeString(releaseDate);
        parcel.writeInt(movieId);
        parcel.writeString(movieType);
    }

    public static final Parcelable.Creator<movieDetails> CREATER
            = new Parcelable.Creator<movieDetails>() {

        @Override
        public movieDetails createFromParcel(Parcel parcel) {
            return new movieDetails(parcel);
        }

        @Override
        public movieDetails[] newArray(int i) {
            return new movieDetails[i];
        }
    };


    public static final Creator<movieDetails> CREATOR = new Creator<movieDetails>() {
        @Override
        public movieDetails createFromParcel(Parcel in) {
            return new movieDetails(in);
        }

        @Override
        public movieDetails[] newArray(int size) {
            return new movieDetails[size];
        }
    };


    public String getOriginalTitle() {
        return originalTitle;
    }

    public int getMovieId(){return movieId;}

    public String getMoviePoster() {
        return moviePoster;
    }


    public String getPlotSynopsis() {
        return plotSynopsis;
    }


    public int getUserRating()
    {
        return userRating;
    }

    public String getReleaseDate() {
        return releaseDate.substring(0 , 4);

    }
    public String getMovieType(){return movieType;}

    public void setMovieType(String movieType){this.movieType = movieType;}

}