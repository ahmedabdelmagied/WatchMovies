package com.example.abdelmagied.moviemapp.Adapters;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.abdelmagied.moviemapp.R;
import com.example.abdelmagied.moviemapp.movieDetails;
import com.example.abdelmagied.moviemapp.reviewdetails;
import com.example.abdelmagied.moviemapp.tailerdetails;
import com.example.abdelmagied.moviemapp.utilites.NetworkUtilites;
import com.squareup.picasso.Picasso;

import java.lang.reflect.Array;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;


/**
 * Created by AbdELMagied on 9/23/2017.
 */
public class movieAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final Context context;
    public movieClickHandler mMovieClickHandler;
    private ArrayList<movieDetails> mMovies;


    public movieAdapter(Context context, ArrayList<movieDetails> mMovies, movieClickHandler mClickHandlerr) {
        this.context = context;
        this.mMovies = mMovies;
        this.mMovieClickHandler = mClickHandlerr;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.movie_item, parent, false);
        return new movieViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewholder, int position) {
                movieDetails mMovieDetails = mMovies.get(position);
                movieViewHolder holder = (movieViewHolder) viewholder;
                String posterPath = mMovieDetails.getMoviePoster();
                // get the full movie poster URL using method buldMoviePosterUrl which we built.
                URL moviePosterUrl = NetworkUtilites.buildMoviePosterUrl(posterPath, "w500");
                // Load the poster image in the movieImage using Picasso
                Picasso.with(context).load(moviePosterUrl.toString()).into(holder.movieImage);

    }

    @Override
    public int getItemCount() {
        if (mMovies == null)
             return 0;
        return mMovies.size();
    }

    public void changeArray(ArrayList<movieDetails> newMovie) {
           this.mMovies = newMovie;
        notifyDataSetChanged();
    }

    public interface movieClickHandler {
        public void handleClick(int number , String movieType , int movieId);
    }



    class movieViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public final ImageView movieImage;

        public movieViewHolder(View itemView) {
            super(itemView);
            movieImage = (ImageView) itemView.findViewById(R.id.containerID);
            itemView.setOnClickListener(this);
        }
        @Override
        public void onClick(View view) {
            mMovieClickHandler.handleClick(getAdapterPosition() , mMovies.get(getAdapterPosition()).getMovieType() , mMovies.get(getAdapterPosition()).getMovieId());
        }
    }
}

