package com.example.abdelmagied.moviemapp.utilites;

import com.example.abdelmagied.moviemapp.movieDetails;
import com.example.abdelmagied.moviemapp.reviewdetails;
import com.example.abdelmagied.moviemapp.tailerdetails;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.ExecutionException;

/**
 * Created by AbdELMagied on 9/23/2017.
 */
public class JsonUtilites {



    /**
     * read the main original title , movie poster image , plot synopsis  , user rating , release data
     *
     * from the jsonObject and put it in ArrayList ..
     *
     * @String jsonString.
     * @return ArrayList of movieDetails.
     */
    public static ArrayList<movieDetails> readJsonData(String jsonMovies , String movieType) throws JSONException, IOException, ExecutionException, InterruptedException {
        JSONObject root       = new JSONObject(jsonMovies);
        JSONArray resultMovie = root.getJSONArray("results");
        ArrayList<movieDetails> movieReturnDetails = new ArrayList<movieDetails>();
        for(int i = 0 ; i < resultMovie.length(); i++){

            JSONObject singleMovie = resultMovie.getJSONObject(i);
            movieDetails movie = returnMovieDetails(singleMovie , movieType);
            movieReturnDetails.add(movie);
        }
        return movieReturnDetails;
    }


    /**
     * a simple method to make movieDetails object from the singleMovie
     * the benefit of this method is to prevent Repetition of the code..
     *
     * @param singleMovie
     * @return
     * @throws JSONException
     */
    public static movieDetails returnMovieDetails(JSONObject singleMovie , String movieType) throws JSONException, IOException, ExecutionException, InterruptedException {

        String originalTitle   = singleMovie.getString("original_title");
        String moviePoster     = singleMovie.getString("poster_path");
        String overView        = singleMovie.getString("overview");
        String releaseData     = singleMovie.getString("release_date");
        int    userRating      = singleMovie.getInt("vote_average");
        int    movieId         = singleMovie.getInt("id");

        return new movieDetails(originalTitle , moviePoster , overView , userRating , releaseData , movieId , movieType);
    }




}
