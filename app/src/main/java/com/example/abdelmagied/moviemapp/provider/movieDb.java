package com.example.abdelmagied.moviemapp.provider;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by AbdELMagied on 9/25/2017.
 */
public class movieDb extends SQLiteOpenHelper {


    public movieDb(Context context) {
        super(context, movieContract.MOVIE_DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        String DATABASE_QUERY = "CREATE TABLE "               + movieContract.MovieEntry.TABLE_NAME + " ( " +
                movieContract.MovieEntry._ID                  + " INTEGER PRIMARY KEY AUTOINCREMENT  , " +
                movieContract.MovieEntry.COLUMN_MOVIE_TITLE   + " TEXT , " +
                movieContract.MovieEntry.COLUMN_MOVIE_ID      + " INTEGER )";

        sqLiteDatabase.execSQL(DATABASE_QUERY);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

          sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + movieContract.MovieEntry.TABLE_NAME);
          onCreate(sqLiteDatabase);
    }
}
