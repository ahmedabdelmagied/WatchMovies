package com.example.abdelmagied.moviemapp.provider;

import android.net.Uri;
import android.provider.BaseColumns;

import java.net.URI;

/**
 * Created by AbdELMagied on 9/25/2017.
 */
public class movieContract {

    public static final String AUTHORITY               = "com.example.abdelmagied.moviemapp";

    public static final Uri BASE_CONTENT_URI           = Uri.parse("content://" + AUTHORITY);

    public static final String MOVIE_PATH              = "movie";

    public static final String MOVIE_DATABASE_NAME     = "movieDB";


    public static final class MovieEntry implements BaseColumns{

        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(MOVIE_PATH).build();

        public static final String TABLE_NAME            = "movie";
        public static final String COLUMN_MOVIE_ID       = "movieId";
        public static final String COLUMN_MOVIE_TITLE    = "movieTitle";
    }
}
