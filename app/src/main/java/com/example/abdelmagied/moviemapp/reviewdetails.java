package com.example.abdelmagied.moviemapp;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by AbdELMagied on 9/26/2017.
 */
public class reviewdetails{
    public String author;
    public String content;

    public reviewdetails(String author , String content) {
        this.author   = author;
        this.content  = content;
    }

    public String getAuthor() {
        return author;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

}
