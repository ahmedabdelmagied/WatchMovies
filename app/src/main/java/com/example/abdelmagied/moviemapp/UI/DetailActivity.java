
package com.example.abdelmagied.moviemapp.UI;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.abdelmagied.moviemapp.Adapters.reviewAdapter;
import com.example.abdelmagied.moviemapp.R;
import com.example.abdelmagied.moviemapp.Adapters.movieAdapter;
import com.example.abdelmagied.moviemapp.movieDetails;
import com.example.abdelmagied.moviemapp.provider.movieContract;
import com.example.abdelmagied.moviemapp.reviewdetails;
import com.example.abdelmagied.moviemapp.Adapters.tailerAdapter;
import com.example.abdelmagied.moviemapp.tailerdetails;
import com.example.abdelmagied.moviemapp.utilites.MovieUtilites;
import com.example.abdelmagied.moviemapp.utilites.NetworkUtilites;
import com.squareup.picasso.Picasso;

import org.json.JSONException;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

public class DetailActivity extends AppCompatActivity implements tailerAdapter.ClickhandleTailer {

    private TextView pTitle ;
    private TextView pDate;
    private TextView pOverflow;
    private TextView pRating;
    private ImageView pImage;
    private movieDetails movie;
    private RecyclerView mRecyclerView , mRecyclerView2;
    private reviewAdapter mAdapter;
    private tailerAdapter mTailerAdapter;
    private int movieId;
    ArrayList<tailerdetails> tailer;
    ArrayList<reviewdetails> review;
    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_detail);

        movie= getIntent().getParcelableExtra("movie");
        URL myurl = NetworkUtilites.buildMoviePosterUrl(movie.getMoviePoster() , "w500");

        pTitle    = (TextView) findViewById(R.id.posterTitle);
        pDate     = (TextView) findViewById(R.id.posterDate);
        pOverflow = (TextView) findViewById(R.id.posterOverview);
        pRating   = (TextView) findViewById(R.id.posterRating);
        pImage    = (ImageView) findViewById(R.id.posterContainer);


        pTitle.setText(movie.getOriginalTitle());
        pDate.setText(movie.getReleaseDate());
        pOverflow.setText(movie.getPlotSynopsis());
        pRating.setText(String.valueOf(movie.getUserRating()));
        Picasso.with(this).load(myurl.toString()).into(pImage);
        movieId = movie.getMovieId();

        try {
            tailer =   new getTailerMovie().execute(movieId).get();
            review =   new getReviewMovie().execute(movieId).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        mRecyclerView = (RecyclerView) findViewById(R.id.reviewRecycler);
        mAdapter       = new reviewAdapter(this , review);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));


        mRecyclerView = (RecyclerView) findViewById(R.id.tailerRecyclerView);
        mTailerAdapter      = new tailerAdapter(this , tailer , this);
        mRecyclerView.setAdapter(mTailerAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
    }

    public void addToFavourite(View view) {
        ContentValues contents = new ContentValues();
        contents.put(movieContract.MovieEntry.COLUMN_MOVIE_ID , movieId);
        contents.put(movieContract.MovieEntry.COLUMN_MOVIE_TITLE , pTitle.getText().toString());

        getContentResolver().insert(movieContract.MovieEntry.CONTENT_URI , contents);
        Toast.makeText(getApplicationContext() , "Successfly added to favourite List " , Toast.LENGTH_LONG).show();
    }

    /**
     * function that responsible to launch tailer in the youtube .
     * @param position is the position of the tailer that will lanuched in the youtube
     */
    @Override
    public void handleTailerClick(int position) throws MalformedURLException {
        String url = tailer.get(position).getYoutube_tailer().toString();
        Uri webpage = Uri.parse(url);
        Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }


    /**
     *  AsynTask to get the tailers of a specific movie
     *  the input is the ID of the movie
     *  the output is ArrayList<tailerdetails> of the provided movieID
     */
    class getTailerMovie extends AsyncTask<Integer, Void , ArrayList<tailerdetails> >{
           @Override
           protected ArrayList<tailerdetails> doInBackground(Integer... integers) {
               int id = integers[0];

               ArrayList<tailerdetails> tailerdetails = null;
               try {
                   tailerdetails = MovieUtilites.constructTailerList(id);
               } catch (IOException e) {
                   e.printStackTrace();
               } catch (JSONException e) {
                   e.printStackTrace();
               } catch (ExecutionException e) {
                   e.printStackTrace();
               } catch (InterruptedException e) {
                   e.printStackTrace();
               }
               return tailerdetails;
           }
       }


    /**
     * AsynTask to get the reviews of a specific movie
     * input is the ID of movie
     * the output is ArrayList<reviewdetails> that contain the reviews of movie
     */
    class getReviewMovie extends AsyncTask<Integer, Void , ArrayList<reviewdetails> >{
        @Override
        protected ArrayList<reviewdetails> doInBackground(Integer... integers) {
            int id = integers[0];

            ArrayList<reviewdetails> review = null;
            try {
                review = MovieUtilites.constructReviewList(id);

            } catch (JSONException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return review;
        }
    }
}
