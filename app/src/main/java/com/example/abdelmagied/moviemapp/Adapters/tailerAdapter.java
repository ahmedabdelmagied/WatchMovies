package com.example.abdelmagied.moviemapp.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.abdelmagied.moviemapp.R;
import com.example.abdelmagied.moviemapp.tailerdetails;

import java.net.MalformedURLException;
import java.util.ArrayList;

/**
 * Created by AbdELMagied on 9/28/2017.
 */
public class tailerAdapter extends RecyclerView.Adapter<tailerAdapter.ViewHolder> {
    Context context;

    ArrayList<tailerdetails> tailers;
    ClickhandleTailer mClickHandler;
    public interface ClickhandleTailer{
        public void handleTailerClick(int position) throws MalformedURLException;
    }

    public tailerAdapter (Context context , ArrayList<tailerdetails> tailers , ClickhandleTailer mclickhandler){
        this.context  = context ;
        this.tailers = tailers;
        this.mClickHandler = mclickhandler;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.tailerview , parent , false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
           holder.tailerTitle.setText(tailers.get(position).getTailerTitle());
    }

    @Override
    public int getItemCount() {
        if(tailers != null)
            return tailers.size();
        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public final TextView tailerTitle;
        public ViewHolder(View itemView) {
            super(itemView);
            tailerTitle = (TextView) itemView.findViewById(R.id.tailerid);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            try {
                mClickHandler.handleTailerClick(getAdapterPosition());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }
    }
}
