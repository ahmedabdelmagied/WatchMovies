package com.example.abdelmagied.moviemapp.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.abdelmagied.moviemapp.R;
import com.example.abdelmagied.moviemapp.reviewdetails;
import com.example.abdelmagied.moviemapp.tailerdetails;

import java.util.ArrayList;

/**
 * Created by AbdELMagied on 9/28/2017.
 */
public class reviewAdapter extends RecyclerView.Adapter<reviewAdapter.ViewHolder>{
    Context context;

    ArrayList<reviewdetails> reviews;

    public reviewAdapter (Context context , ArrayList<reviewdetails> reviews){
        this.context  = context ;
        this.reviews  = reviews;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.review_item , parent , false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        reviewdetails mreview = reviews.get(position);
        String author = mreview.getAuthor();
        String content = mreview.getContent();
        holder.author.setText(author);
        holder.content.setText(content);
    }

    @Override
    public int getItemCount() {
        if(reviews != null)
            return reviews.size();
        return 0;
    }


    public class ViewHolder extends RecyclerView.ViewHolder{
        public TextView author;
        public TextView content;

        public ViewHolder(View itemView) {
            super(itemView);
            author = (TextView) itemView.findViewById(R.id.authorId);
            content = (TextView) itemView.findViewById(R.id.contentId);
        }

    }
}
