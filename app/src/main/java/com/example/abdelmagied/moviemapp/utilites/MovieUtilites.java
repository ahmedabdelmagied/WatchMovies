package com.example.abdelmagied.moviemapp.utilites;

import android.os.AsyncTask;

import com.example.abdelmagied.moviemapp.BuildConfig;
import com.example.abdelmagied.moviemapp.movieDetails;
import com.example.abdelmagied.moviemapp.reviewdetails;
import com.example.abdelmagied.moviemapp.tailerdetails;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.TreeMap;
import java.util.concurrent.ExecutionException;

/**
 * Created by AbdELMagied on 9/25/2017.
 */
public class MovieUtilites  {

    public static ArrayList<movieDetails> favouriteMovies;
    private static String jsonTailer;
    private static String API_KEY = BuildConfig.API_KEY;


    /**
     * function that used to construct the favourite list
     * @param allMovies contain alist of allmovies
     * @param favouriteMoviesIDS  contain the ids of the favourite movies
     * @return
     */
    public static ArrayList<movieDetails> favouriteListConstruction(ArrayList<movieDetails> allMovies , int[] favouriteMoviesIDS)
    {
        ArrayList<movieDetails> movieReturnDetails = new ArrayList<>();
        if(favouriteMoviesIDS == null)
            return movieReturnDetails;
        for(int i = 0 ; i < allMovies.size(); i++){
            final int  movieID  = allMovies.get(i).getMovieId();
            boolean foundId = false;

            // iterate over the movieIds to ensure if the current movie id is found in the movieIds or not..
            for(int j = 0 ; j < favouriteMoviesIDS.length ; j++){

                if(favouriteMoviesIDS[j] == movieID){
                    foundId = true;
                    break;
                }
            }
            // if we found that the movie id is found in the movieIds that add it to the returned arraylist
            if(foundId == true) {
                movieReturnDetails.add(allMovies.get(i));
            }
        }
        favouriteMovies = movieReturnDetails;
        return movieReturnDetails;
    }


    /**
     *  function that is used to construct Tailer list of a specific movie id
     *  @param movieId the id of the movie
     *  @return a list that contain the tailers of the movie
     *
     */
    public static ArrayList<tailerdetails> constructTailerList(int movieId) throws IOException, JSONException, ExecutionException, InterruptedException {
         URL tailerurl = NetworkUtilites.buildTailerReviewsUrl("videos" , API_KEY , movieId);
        jsonTailer =NetworkUtilites.getResponseFromHttpRequest(tailerurl);

        JSONObject root       = new JSONObject(jsonTailer);
        JSONArray resultMovie = root.getJSONArray("results");
        ArrayList<tailerdetails> movieTailerDetails = new ArrayList<>();

        for(int i = 0 ; i < resultMovie.length(); i++)
        {
            JSONObject singleMovie = resultMovie.getJSONObject(i);
            String key             = singleMovie.getString("key");
            String title           = singleMovie.getString("name");
            movieTailerDetails.add(new tailerdetails(key , title));
        }

        return movieTailerDetails;
    }


    /**
     * function that is used to construct reviews list of a specific movie id
     * @param movieId the id of the movie
     * @return a list that contain the reviews of the movie
     *
     */
    public static ArrayList<reviewdetails> constructReviewList(int movieId) throws ExecutionException, InterruptedException, JSONException, IOException {
        URL reviewurl = NetworkUtilites.buildTailerReviewsUrl("reviews" , API_KEY , movieId);
        jsonTailer    = NetworkUtilites.getResponseFromHttpRequest(reviewurl);

        JSONObject root       = new JSONObject(jsonTailer);
        JSONArray resultMovie = root.getJSONArray("results");
        ArrayList<reviewdetails> movieReviewDetails = new ArrayList<>();

        for(int i = 0 ; i < resultMovie.length(); i++)
        {
            JSONObject singleMovie = resultMovie.getJSONObject(i);
            String author          = singleMovie.getString("author");
            String content         = singleMovie.getString("content");
            movieReviewDetails.add(new reviewdetails(author , content));
        }

        return movieReviewDetails;
    }
}
