package com.example.abdelmagied.moviemapp.utilites;

import android.content.Context;
import android.net.Uri;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

/**
 * Created by AbdELMagied on 9/23/2017.
 */
public class NetworkUtilites {

        private final static String Movie_APi = "http://api.themoviedb.org/3/movie";
    private final static String BASE_POSTER_URI = "http://image.tmdb.org/t/p";


    /**
     * build the URL that use to request the ( popular / top_rated )  movies.
     *
     * @param requestType of the popular movie it can be ( popular ) or  ( top_rated ).
     * @param API_KEY     is the API key of the MovieDB API.
     * @return the URL that use to query requestType ( popular / top_rated) movies.
     */
    public static URL buildUrlForPopularMove(String requestType, String API_KEY) {

        Uri buildUri = Uri.parse(Movie_APi).buildUpon()
                .appendPath(requestType)
                .appendQueryParameter("api_key", API_KEY)
                .build();

        URL myUrl = null;

        try {
            myUrl = new URL(buildUri.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        return myUrl;
    }



    /**
     * method used to build the url of tailer ro reviews according to the urlType
     * if urlType = "video" then build the url of video else if urltype  = review
     * then build the url of the reviews
     * @param urlType can by "video" or "reviews"
     * @param API_KEY
     * @param movieId the id of the movie
     * @return the url of the video or reviews according to the urlType
     */
    public static URL buildTailerReviewsUrl(String urlType, String API_KEY, int movieId)
    {
        Uri buildUri  = Uri.parse(Movie_APi).buildUpon()
                .appendPath(String.valueOf(movieId))
                .appendPath(urlType)
                .appendQueryParameter("api_key" , API_KEY)
                .build();

        URL myUrl = null;

        try {
            myUrl = new URL(buildUri.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        return myUrl;
    }



    /**
     * this method return the entire method from HTTP request
     *
     * @param url the URL to fetch the HTTP response from.
     * @return the content of HTTP request.
     * @throws IOException related to network and stream Reading.
     */
    public static String getResponseFromHttpRequest(URL url) throws IOException{

         HttpURLConnection urlConnection  = (HttpURLConnection) url.openConnection();

         try {

             InputStream inputStream = urlConnection.getInputStream();

             Scanner scanner = new Scanner(inputStream);
             scanner.useDelimiter("\\A");

             if(scanner.hasNext()){
                 return scanner.next();
             }

             return "l";
         }finally {
              urlConnection.disconnect();
         }
    }


    /**
     * this method build the complete path of the movie poster.
     * @param posterPath the path of the poster.
     * @param posterSize the size of the poster.
     * @return return the URL of the movie poster.
     */
    public static URL buildMoviePosterUrl(String posterPath , String posterSize){

       Uri buildUri = Uri.parse(BASE_POSTER_URI).buildUpon()
               .appendEncodedPath(posterSize)
               .appendEncodedPath(posterPath)
               .build();

        URL url = null;
        try{
            url = new URL(buildUri.toString());
        }catch (MalformedURLException e)
        {
            e.printStackTrace();
        }
        return url;
    }
}

