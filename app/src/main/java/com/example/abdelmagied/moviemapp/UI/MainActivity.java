package com.example.abdelmagied.moviemapp.UI;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.example.abdelmagied.moviemapp.BuildConfig;
import com.example.abdelmagied.moviemapp.R;
import com.example.abdelmagied.moviemapp.Adapters.movieAdapter;
import com.example.abdelmagied.moviemapp.movieDetails;
import com.example.abdelmagied.moviemapp.provider.movieContract;
import com.example.abdelmagied.moviemapp.reviewdetails;
import com.example.abdelmagied.moviemapp.tailerdetails;
import com.example.abdelmagied.moviemapp.utilites.JsonUtilites;
import com.example.abdelmagied.moviemapp.utilites.MovieUtilites;
import com.example.abdelmagied.moviemapp.utilites.NetworkUtilites;

import org.json.JSONException;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

public class    MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor>, movieAdapter.movieClickHandler {

    private ArrayList<movieDetails> allMovies;
    private ArrayList<movieDetails> popularMovies;
    private ArrayList<movieDetails> topRatedMovies;
    private int AdapterPosition = -1;
    private String JsonString;
    private RecyclerView mRecyclerView;
    private movieAdapter mAdapter;
    private static final int MOVIE_lOADER = 55;
    private String API_KEY = BuildConfig.API_KEY;
    private String CurrentSortSelection = "Popular";
    private int [] favoriteMovieIDs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Spinner mSpinner = (Spinner) findViewById(R.id.spinnerId);
        Toolbar mytool = (Toolbar) findViewById(R.id.toolbarId);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.SortByOptions, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinner.setAdapter(adapter);
        if (mytool != null) {
            setSupportActionBar(mytool);
        }

        getSupportLoaderManager().initLoader(MOVIE_lOADER, null, this);
        loadDateToArrays("popular");

        // Get the RecyclerView .
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerID);
        mAdapter = new movieAdapter(this, popularMovies,this);
        // Set the adapter to the RecyclerView.
        mRecyclerView.setAdapter(mAdapter);

        // in landscope mode make 3 movies
        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
            mRecyclerView.setLayoutManager(new GridLayoutManager(this, 3));
        }else
         mRecyclerView.setLayoutManager(new GridLayoutManager(this, 2));



        if(savedInstanceState != null && savedInstanceState.containsKey("adapterPosition")){
            AdapterPosition = savedInstanceState.getInt("adapterPosition");
            mRecyclerView.scrollToPosition(AdapterPosition);
        }


        // handle the spinner item selection..
        mSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Resources res = getResources();
                String arr[] = res.getStringArray(R.array.SortByOptions);
                String selectedItem = arr[i];

                if (selectedItem.equals("Popular")) {
                    CurrentSortSelection = "Popular";
                    mAdapter.changeArray(popularMovies);

                }else if (selectedItem.equals("Top rated")) {
                    CurrentSortSelection = "Top rated";
                    if(topRatedMovies == null){
                        loadDateToArrays("top_rated");
                    }
                    mAdapter.changeArray(topRatedMovies);
                }else if(selectedItem.equals("Favorites")) {
                    CurrentSortSelection = "Favorites";
                    allMovies = new ArrayList<>(popularMovies);
                    if(topRatedMovies != null)
                        allMovies.addAll(topRatedMovies);
                    ArrayList<movieDetails> favouriteMovies = MovieUtilites.favouriteListConstruction(allMovies , favoriteMovieIDs);
                    mAdapter.changeArray(favouriteMovies);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }


    /**
     * I made this function in order to load movie Details in ArrayList as an object of movieDetails
     * this is done to prevent loading data every time user change the sort type (popular , top_rated , favourite)
     * and prevent code duplication
     * @param type can be "popular"  or "top_rated"
     */
    public void loadDateToArrays(String type)
    {
        URL url = NetworkUtilites.buildUrlForPopularMove(type, API_KEY);
        try{
            JsonString = new mytask().execute(url).get();

            if(type == "popular"){
                popularMovies = JsonUtilites.readJsonData(JsonString , "popular");
            }else if(type == "top_rated"){
                topRatedMovies = JsonUtilites.readJsonData(JsonString , "top_rated");
            }

        }catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if(AdapterPosition != -1)
            outState.putInt("adapterPosition" , AdapterPosition);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        AdapterPosition = savedInstanceState.getInt("adapterPostion");
    }

    /**
     * handle the click of the movie in the recyclerview and lunch the DetailsActivity of the class
     *
     * @param number
     */
    @Override
    public void handleClick(int number , String movieType , int movieId) {


        AdapterPosition = number;
        movieDetails movie = null;
        ArrayList<movieDetails> movies = new ArrayList<>();

        if(movieType.equals("popular")) {
           movie = popularMovies.get(number);
        } if(movieType.equals("top_rated")){
           movie = topRatedMovies.get(number);
        }if(CurrentSortSelection.equals("Favorites")){
               if(movieType.equals("popular"))
               {
                   for(int i = 0 ; i < popularMovies.size() ; i++){
                       if(popularMovies.get(i).getMovieId() == movieId){
                           movie = popularMovies.get(i);
                           break;
                       }
                   }
               }else{
                    for(int i = 0 ; i < topRatedMovies.size() ; i++){
                         if(topRatedMovies.get(i).getMovieId() == movieId){
                             movie = topRatedMovies.get(i);
                             break;
                         }
                    }
               }
        }


        // make an intent to go to the DetailActivity
        Intent myIntent  = new Intent(getApplicationContext(), DetailActivity.class);
        myIntent.putExtra("movie", movie);
        startActivity(myIntent);
    }


    class mytask extends AsyncTask<URL, Void, String> {
        @Override
        protected String doInBackground(URL... urls) {
            URL myurl = urls[0];
            String q = "";
            try {
                q = NetworkUtilites.getResponseFromHttpRequest(myurl);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return q;
        }
    }



    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Uri MOVIE_URI = movieContract.BASE_CONTENT_URI.buildUpon().appendPath(movieContract.MOVIE_PATH).build();
        if(id == MOVIE_lOADER){
            return new CursorLoader(
                    this ,
                    MOVIE_URI ,
                    null ,
                    null ,
                    null,
                    null
            );
        }
        return null;
    }


    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if(data != null) {
            favoriteMovieIDs = new int[data.getCount()];
            // iterate over the cursor and save the ids in favouriteMovieIDs array
            for (int i = 0; i < data.getCount(); i++) {
                data.moveToPosition(i);
                int movieId = data.getInt(data.getColumnIndex(movieContract.MovieEntry.COLUMN_MOVIE_ID));
                favoriteMovieIDs[i] = movieId;
            }
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }


}
