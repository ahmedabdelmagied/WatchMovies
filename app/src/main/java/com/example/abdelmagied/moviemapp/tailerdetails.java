package com.example.abdelmagied.moviemapp;

import android.os.Parcel;
import android.os.Parcelable;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by AbdELMagied on 9/26/2017.
 */
public class tailerdetails{
    private String tailerkey;
    private String tailerTitle;
    private URL youtube_tailer;

    public tailerdetails(String tailerkey, String tailerTitle) throws MalformedURLException {
        this.tailerkey      = tailerkey;
        this.tailerTitle    = tailerTitle;
        this.youtube_tailer = buildYoutubeURl();
    }

    protected tailerdetails(Parcel in) {
        tailerkey   = in.readString();
        tailerTitle = in.readString();
    }

    public URL buildYoutubeURl() throws MalformedURLException {
        return  new URL("https://www.youtube.com/watch?v=" + tailerkey);
    }



    public String getTailerkey() {
        return tailerkey;
    }

    public void setTailerkey(String tailerkey) {
        this.tailerkey = tailerkey;
    }

    public String getTailerTitle() {
        return tailerTitle;
    }

    public void setTailerTitle(String tailerTitle) {
        this.tailerTitle = tailerTitle;
    }

    public URL getYoutube_tailer() {
        return youtube_tailer;
    }

    public void setYoutube_tailer(URL youtube_tailer) {
        this.youtube_tailer = youtube_tailer;
    }
}
